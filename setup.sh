#!/bin/bash

oc new-build python:3.6 --binary=true --name=python-app --strategy=source
oc start-build python-app --from-dir=. --follow
oc new-app python-app
oc scale dc python-app --replicas=0

oc set volume dc python-app --add --name=python-app-pv -t pvc --claim-size=1G --mount-path=/opt/app-root/src/pv



