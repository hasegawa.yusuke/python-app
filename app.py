import os
import time
import datetime

with open(os.environ.get('HOME', '/tmp') + '/pv/input.txt','r') as reader:
    input_data = reader.read().splitlines()
    print('input_data from input.txt: ' + ''.join(str(e) for e in input_data))

with open(os.environ.get('HOME', '/tmp') + '/pv/output.txt','a+') as writer:
    for input_line in input_data:
        writer.write(input_line + ' written by writer@' + str(datetime.datetime.now()) + '\n')

with open(os.environ.get('HOME', '/tmp') + '/pv/log.txt','a+', buffering=1) as writer:        
    print("--- Training Start ---")
    writer.write('--- Training Start ---\n')   
    while True:
        writer.write('log message@' + str(datetime.datetime.now()) + '\n')
        print('log message@' + str(datetime.datetime.now()))        
        time.sleep(3)
        
    




    
    



