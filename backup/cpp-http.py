from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    with open('/opt/app-root/src/pv/input.txt','r') as reader:
        input_data = reader.read().splitlines()

    with open('/opt/app-root/src/pv/output.txt','a+') as writer:
        for str in input_data:
            writer.write(str + ' written by writer\n')

    return "Hello, World!"
    
if __name__ == "__main__":
    app.run(debug=False)
    
    



